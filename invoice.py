#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL
from trytond.pool import Pool


class Line(ModelSQL, ModelView):
    _name = 'account.invoice.line'

    def _get_tax_rule_pattern(self, party, vals):
        '''
        Get tax rule pattern

        :param party: the BrowseRecord of the party
        :param vals: a dictionary with value from on_change
        :return: a dictionary to use as pattern for tax rule
        '''
        product_obj = Pool().get('product.product')

        res = super(Line, self)._get_tax_rule_pattern(party, vals)
        if vals.get('product'):
            product = product_obj.browse(vals['product'])
            res['product_type_tax'] = product.type_tax.id
        return res

Line()
