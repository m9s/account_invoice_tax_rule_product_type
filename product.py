#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields


class TypeTax(ModelSQL, ModelView):
    'Product Type Tax'
    _name = 'product.type_tax'
    _description = __doc__

    name = fields.Char('Name', required=True, translate=True, loading='lazy')
    code = fields.Char('Code', required=True)

TypeTax()


class Template(ModelSQL, ModelView):
    _name = "product.template"

    type_tax = fields.Many2One('product.type_tax', 'Type Tax',
            required=True)

Template()
