# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Account Invoice Tax Rule Product Type',
    'name_de_DE': 'Fakturierung Steuerregel Artikeltyp',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''Taxes for Rules on Product Types
    - Adds Taxes for Product Types handled by Rules
''',
    'description_de_DE': '''Steuern für Steuerregeln nach Artikeltypen
    - Fügt Steuern hinzu, die auf Artikeltypen im Zusammenhang mit Steuerregeln
      angewendet werden
''',
    'depends': [
        'account_invoice',
    ],
    'xml': [
        'product.xml',
        'tax.xml',
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
