#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields


class RuleLine(ModelSQL, ModelView):
    _name = 'account.tax.rule.line'

    product_type_tax = fields.Many2One('product.type_tax', 'Product Type Tax')

RuleLine()


class RuleLineTemplate(ModelSQL, ModelView):
    _name = 'account.tax.rule.line.template'

    product_type_tax = fields.Many2One('product.type_tax', 'Product Type Tax')

    def _get_tax_rule_line_value(self, template, rule_line=None):
        '''
        Set values for tax rule line creation.

        :param template: the BrowseRecord of the template
        :param rule_line: the BrowseRecord of the rule line to update
        :return: a dictionary with rule line fields as key and values as value
        '''
        res = super(RuleLineTemplate, self)._get_tax_rule_line_value(template)
        if not rule_line or rule_line.product_type_tax.id != \
                template.product_type_tax.id:
            res['product_type_tax'] = template.product_type_tax.id
        return res

RuleLineTemplate()
